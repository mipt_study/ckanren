/* Idea of using c stack for forth-like language operations */


/* pair structure for emulating linked (lisp-style) lists */
#include<stdio.h>


struct _value {
	char type;
	union {
		void* ptr;
		int val;
		double fval;
	};

};

typedef struct _value Value;
typedef Value Result;

#define VALUE_NULL 0
#define VALUE_FUNC 1
#define VALUE_PTR 2
#define VALUE_VAR 3
#define VALUE_VAL 4


struct _pair {
	Value val;
	struct _pair* tail;
};

typedef struct _pair Pair;
typedef Pair Stack;


/* structure for handling a stream of instructions */
struct _program {
	size_t pos;
	Value code[];

};

typedef struct _program Program;

/* next instruction */

int prog_next(Program* p, Value* v) {
	if ( p->code[p->pos].type != VALUE_NULL ) {
		printf("Reading code at pos %d\n", p->pos);
		*v = p->code[p->pos];
		p->pos++;
		return 1;
	}
	return 0;
}

/* either
 * value => put value on stack && call
 *   while result of call == 0 (nothing to eat) 
 *     if result
 *        put_it on stack instead (replace local value)
 *     call
 *   else
 *     *result = value
 *     return 0
 *     
 * function => return result + number of stack items to eat && return
 */

typedef int (*func_t)(Program* p, Pair* s, Value* r);


int check_stack(Stack* s, int n) {
	int found = 0;
	while ( found < n && s != NULL ) {
		found++;
		s = s->tail;
	}
	if( n != 0 && found < n) {
		printf("Needed stack size: %d, but %d found\n", n, found);
		return -5;
	} 
	return 0;
}


int fplus(Program* p, Pair* s, Value* r) {
	int err = check_stack(s, 2);
	if (err) return err;
	r->type = VALUE_VAL;
	printf("PLUS %d %d\n", s->val.val, s->tail->val.val);	
	r->val = s->val.val + s->tail->val.val;
	return 2;
}

int fminus(Program* p, Pair* s, Value* r) {
	int err = check_stack(s, 2);
	if (err) return err;
	r->type = VALUE_VAL;
	printf("FMINUS %d %d\n", s->val.val, s->tail->val.val);
	r->val = s->val.val - s->tail->val.val;
	return 2;
}

int fmul(Program* p, Pair* s, Value* r) {
	int err = check_stack(s, 2);
	if (err) return err;
	r->type = VALUE_VAL;
	printf("MUL %d %d\n", s->val.val, s->tail->val.val);
	r->val = s->val.val * s->tail->val.val;
	return 2;
}

int fdiv(Program* p, Pair* s, Value* r) {
	int err = check_stack(s, 2);
	if ( err ) return err;
	r->type = VALUE_VAL;
	r->val = s->tail->val.val / s->val.val;
	return 2;
}

int fneg(Program* p, Pair*s, Value* r) {
	int err = check_stack(s, 1);
	if (err) return err;
	r->type = VALUE_VAL;
	r->val = -s->val.val;
	return 1;
}

int dup(Program* p, Pair* s, Value* r) {
	int err = check_stack(s, 1);
	if (err ) return err;
	*r = s->val;
	return 0;
}

int swap(Program* p, Pair* s, Value* r) {
	int err = check_stack(s, 2);
	Value x;
	if (err) return err;
	printf("SWApping\n");
	r->type = VALUE_NULL;
	x = s->val;
	s->val = s->tail->val;
	s->tail->val = x;
	return 0;
}

int drop(Program* p, Pair* s, Value* r) {
	int err = check_stack(s, 1);
	if (err) return err;
	r->type = VALUE_NULL;
	return 1;
}

int over(Program* p, Pair* s, Value* r) {
	int err = check_stack(s, 2);
	if (err) return err;
	*r = s->tail->val;
	return 0;
}


#define FUNC(x) {.type=VALUE_FUNC, .ptr=(x)}


#define FPLUS FUNC(fplus)
#define FMUL  FUNC(fmul)
#define FMINUS  FUNC(fminus)
#define FDIV  FUNC(fdiv)
#define FNEG FUNC(fneg)
#define DUP FUNC(dup)
#define SWAP FUNC(swap)
#define DROP FUNC(drop)
#define OVER FUNC(over)

#define STOP {.type=VALUE_NULL, .ptr=0}


/*
 * -> instruction_step (pos=0, NULL, &result) 
 *  -> prog_next(p)
 *  <- prog_next(p, val=10) = 1
 *  stack=10
 *  -> instruction_step (pos=1, &stack, &result)
 
 *   -> prog_next(pos=1)
 *   <- prog_next(pos=2, val=20) = 1
 *   stack=20
 *   -> instruction_step(pos=2, &stack, &result)
 *    -> prog_next(pos1=2)
 *    <- prog-next(pos1=3, func=1) = 1
 *    -> mplus(&stack, &result)
 *    <- mplus(&stack, result=30) =2 
 *   <- instruction_step(pos=3, &stack, result=30) = 1
 *  <- instrcution_step(pos=3, &stack, result=30) = 0
 */ 


int interp(Program* p, Value v, Stack* s, Result* result) {
	func_t f;
	switch (v.type) {
	case VALUE_FUNC: 
		f = (func_t)v.ptr;
		printf("Calling func\n");
		return f(p, s, result);
	case VALUE_VAR:
	case VALUE_VAL:
		*result = v;
		return 0;
	default:
		/* unknown type */
		return -3;
	}
}


int step(Program* p, Stack* s, Result* result) {
	Value v;
	if (prog_next(p, &v)) {
		int eatup = interp(p, v, s, result);
		while ( eatup == 0 ) {
			if (result->type != VALUE_NULL) {
				Stack ns;
				ns.val = *result;
				ns.tail = s;
				eatup = step(p, &ns, result)-1;
			} else {
				eatup = step(p, s, result);
			}
		}
		printf("Returning eatup=%d, result=%d\n", eatup, result->val);
		return eatup;
	} else {
		printf("Program finished\n");
		return -10;
	}
}

#define INT_VAL(x) {.type=VALUE_VAL, .val=(x)}

static Program PROG = {0, {INT_VAL(3), INT_VAL(-4), FMUL,
	INT_VAL(5), FNEG,
	FPLUS,
	INT_VAL(3), SWAP, FMINUS,
	DUP, FPLUS,
	STOP}};



int main() {
	Value result;
	int err = step(&PROG, NULL, &result);
	printf("Result type: %d, value: %d\n", result.type, result.val);
	return 0;
}
