# ckanren

This is a simplistic C implementation of logic micro kernel as described in [microkanren](http://webyrd.net/scheme-2013/papers/HemannMuKanren2013.pdf)
paper of Jason Hemann and Daniel P. Friedman.

# Main ideas

The main idea of this project is to keep this implementaion as simple as possible, focusing on a minial functionality required to
build a logic-programmig kernel, exposing only a small number of logic primitives, being bullet-proof and correct in terms of memory management, but sill extendable with callbacks.

This kernel may be used as an embedded logic programming subsystem in various projects, especially in knowledge-based systems 
which are mainly implemented in a lanugages other than Prolog or other logic or even functional ones and are not able to embed them or take all their dependencies on board.

The other aim of this project is C-language studying, to help students better undestand the concepts of programming, different approaches and how they implemented through each other.
How the lack of one primitives available in one language influences the expressivenes of code in others.

# Implementation

As stated before, the micro/mini-kanren implementation in its *classic* simple form keeps its simplicity and clearness in functional-programming languages such as Scheme.
It's very difficult to preserve this property of code even in other languages supporting FP such as lua and others.

The C language is a way less expressive then even Go or C#, but we are to show here that some concepts can be presented without significant loss of clarity in C.

## Primitives

Microkanren operates with such concepts as *Goal*, *State*, *Variable* and *Stream*. The Goal means the logic goal which evaluates to true or false in a given State.
The State is a set of Variables with associated values. When the goal evaluates to true, the variables should take their values to accomplish it.
All possible sets of variables comprise a Stream of states.

The basic primitives which can be used to construct goals include __unification__, __conjunction__ and __disjunction__. 

## Assumptions

The C syntax has neither lambdas nor clojures which are used to implement lazy-streams and logic junctions, so we have to either reinvent them in terms of C
or use the strength of C and utilize the ephemeral structures.

While a clojure (used to implement a lazy-streams) explicity distinguishes the time of creation and time of evaluation, we have to make a three-function implementaion, in our case it is *\* init*,  *\*next*  and *\*close* 
functions for each goal. Moreover, the clojure itself captures some state of execution, so we have to represent it explicitly as a goal-specific structure which also adds a portion of verbosity.

We name them with *\*Goal* and *\*Stream* respectively. The *\*Goal* structure handles the creation-time goal specific values such as unification values or sub-goals.
The *\*Stream* structure only handles the stream state -- all information needed to evaluate taking next State from the stream.



