#include<stdio.h>
#include<stdlib.h>
#include<stdint.h>
#include<string.h>

typedef uint64_t Value; 

struct _Result {
    struct _Result* base;
    int nvars;
    Value varmap[];
};

#define VAL_TYPEMASK UINT64_C(0xf000000000000000)
#define VAL_VAR      UINT64_C(0x1000000000000000)
#define VAL_VAL		 UINT64_C(0x2000000000000000)
#define IS_VAR(x) ((x)&VAL_VAR != 0)
#define VAL_NULL	UINT64_C(0)
#define VAR_NUM(x)	((x)&(~VAL_TYPEMASK))


typedef struct _Result Result;

struct _Goal {
	struct _Stream* (*init)(struct _Goal* g, Result* r);
};

typedef struct _Goal Goal;



typedef struct _Stream Stream;

struct _Stream {
	int nil;
	int (*next)(struct _Stream*, Result**);
	int (*close)(struct _Stream*);
};


#define INIT_STREAM_BASE(_stream, _next, _close) {\
	_stream->base.next = _next;\
	_stream->base.close = _close;\
	_stream->base.nil = 0; }



/*
 * init(goal, result) -> stream
 * next(stream) -> result
 *
 */

/** interface function init */

Stream* sinit(Goal* g, Result* r) {
    return g->init(g, r);    
}


/** interface function next */

int snext(Stream* s, Result** r) {

    int result;
    
    /* do not call stream if already end of stream */
    if ( s->nil ) return 0;

    result = s->next(s, r);

    /* update end of stream (nil) flag */
    if ( !result )
        s->nil = 1;

    return result;
}


/** interface function close.
 * closes the stream
 */

int sclose(Stream* s) {
	if (s->close)
		return s->close(s);
	/* free allocated stream */
	free(s);
	return 0;
}


/* Disjunction goal description */


/* Disjunction goal structure */

struct _DisjGoal {
	Goal base;
	Goal* x;
	Goal* y;
};



typedef struct _DisjGoal DisjGoal;

/* Disjunction stream structure */

struct _DisjStream {
	Stream base;
    Stream* x;
    Stream* y;
};

typedef struct _DisjStream DisjStream;



int disj_close(Stream* s) {
	DisjStream* stream = (DisjStream*)s;
	sclose(stream->x);
	sclose(stream->y);
	return 0;
}

int disj_next(Stream* s, Result** result) {
	DisjStream* stream = (DisjStream*)s;
	if ( ! stream->x->nil ) {
        /* swap streams */
		Stream* x = stream->x;
		stream->x = stream->y;
		stream->y = x;
	}
	return snext(stream->y, result);
}

Stream* disj_init(Goal* g, Result* result) {
	DisjStream* stream  = malloc(sizeof(DisjStream));
	DisjGoal* goal = (DisjGoal*)g;
	INIT_STREAM_BASE(stream, disj_next, disj_close)
	stream->x = sinit(goal->x, result);
	stream->y = sinit(goal->y, result);
	return (Stream*)stream;
}



struct _ConjGoal {
	Goal base;
	Goal* x;
	Goal* y;
};

typedef struct _ConjGoal ConjGoal;

/* Conjunction stream structure */


struct _ConjStream {
    Stream base;
    Stream* x;
    Stream* y;
    Goal* gy; /** inner goal */
	Result* result; /** current result of outer stream */
};

typedef struct _ConjStream ConjStream;


int conj_next(Stream* s, Result** result) {
	ConjStream* stream = (ConjStream*)s;
	if ( stream->y == NULL ) {
		/* inner cycle is not initialized */
		if ( !snext(stream->x, &stream->result) ) 
			/* the outer cycle is finished */
			return 0;
		stream->y = sinit(stream->gy, stream->result);
	}
	if ( !snext(stream->y, result) )  {
		/* inner cycle is finished */
	 	/* reset the inner cycle */
		sclose(stream->y);
		stream->y = NULL;
		return conj_next(s, result);
	}
	/* else, continue */
	return 1;
}

int conj_close(Stream* s) {
	ConjStream* stream = (ConjStream*) s;
	sclose(stream->x);
	sclose(stream->y);
	return 0;
}

Stream* conj_init(Goal* g, Result* result) {
	ConjStream* stream = malloc(sizeof(ConjStream));
	ConjGoal* goal = (ConjGoal*)g;
	INIT_STREAM_BASE(stream, conj_next, conj_close);
	/* TODO: implement selection of x/y order for optimization */
	/* x -- is an 'outer' cycle */
	stream->x = sinit(goal->x, result);
	/* gy -- is an 'inner' cycle goal*/
	stream->gy = goal->y;
	stream->y = NULL;
	return (Stream*)stream;

}







/* returns corresponsing value for var u
 * or u itself if there is not any association
 */

Value walk(Result* result, Value u) {
    if ( IS_VAR(u) && VAR_NUM(u) < result->nvars ) {
        Value x = result->varmap[VAR_NUM(u)];
        if (x)
            return walk(result, x);
    }
    return u;
}

Result* result_init(int nvars) {
	int i;
	size_t sz = sizeof(Result) + sizeof(Value)*nvars;
	Result* result = malloc(sz);
	if ( !result ) return NULL;
	
	result->base = NULL;
	result->nvars = nvars;
	for ( i = 0; i < nvars; i++ ) {
		result->varmap[i] = VAL_NULL;
	}
	return result;
}


Result* result_clone(Result* res) {
    size_t sz = sizeof(Result)+sizeof(Value)*res->nvars;
    Result* newres = malloc(sz);
    memcpy(newres, res, sz);
    newres->base = res;
    return newres;
}


void result_update(Result** result, Value u, Value v) {
    Result* newres = result_clone(*result);
    newres->varmap[VAR_NUM(u)] = v;
    *result = newres;
}


int unify(Value u, Value v, Result** result) {
    u = walk(*result, u);
    v = walk(*result, v);
    if( u == v) {
        // nothing to unify
        return 1;
    } else if ( IS_VAR(u) && VAR_NUM(u) < (*result)->nvars ) {
        result_update(result, u, v);
        return 1;
    } else if ( IS_VAR(v) && VAR_NUM(u) < (*result)->nvars ) {
        result_update(result, v, u);
        return 1;
    }
    return 0;
}


/* equation goal */

typedef struct {
	Goal base;
	Value u;
	Value v;
} EqGoal;

typedef struct {
	Stream base;
	Value u;
	Value v;
	Result* result;
	int unified;
} EqStream;

int eq_next(Stream* s, Result** result) {
    EqStream* stream = (EqStream*)s;
    // no more results
    if ( stream->unified )
        return 0;
    if (unify(stream->u, stream->v, result )) {
		stream->unified = 1;
        return 1;
    }
    return 0;
}


Stream* eq_init(Goal* goal, Result* result) {
    Stream* stream = malloc(sizeof(EqStream));
	EqGoal* g = (EqGoal*)goal;
    EqStream* eq = (EqStream*) stream;
	INIT_STREAM_BASE(eq, eq_next, NULL);
    eq->u = g->u;
    eq->v = g->v;
    eq->unified = 0;
    return stream;
}

